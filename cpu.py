"""
/*
 * Copyright 2018 Jacob Lifshay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
`timescale 1ns / 1ps
`include "riscv.vh"
`include "cpu.vh"
"""

import string
from migen import *
from migen.fhdl import verilog
from migen.fhdl.structure import _Operator

from riscvdefs import *
from cpudefs import *

class MemoryInterface:
    fetch_address = Signal(32, name="memory_interface_fetch_address") # XXX [2:]
    fetch_data = Signal(32, name="memory_interface_fetch_data")
    fetch_valid = Signal(name="memory_interface_fetch_valid")
    rw_address= Signal(32, name="memory_interface_rw_address") # XXX [2:]
    rw_byte_mask = Signal(4, name="memory_interface_rw_byte_mask")
    rw_read_not_write = Signal(name="memory_interface_rw_read_not_write")
    rw_active = Signal(name="memory_interface_rw_active")
    rw_data_in = Signal(32, name="memory_interface_rw_data_in")
    rw_data_out = Signal(32, name="memory_interface_rw_data_out")
    rw_address_valid = Signal(name="memory_interface_rw_address_valid")
    rw_wait = Signal(name="memory_interface_rw_wait")


class Decoder:
    funct7 = Signal(7, name="decoder_funct7")
    funct3 = Signal(3, name="decoder_funct3")
    rd = Signal(5, name="decoder_rd")
    rs1 = Signal(5, name="decoder_rs1")
    rs2 = Signal(5, name="decoder_rs2")
    immediate = Signal(32, name="decoder_immediate")
    opcode = Signal(7, name="decoder_opcode")
    act = Signal(decode_action, name="decoder_action")


class MStatus:
    def __init__(self, comb, sync):
        self.comb = comb
        self.sync = sync
        self.mpie = Signal(name="mstatus_mpie")
        self.mie = Signal(name="mstatus_mie")
        self.mstatus = Signal(32, name="mstatus")

        self.sync += self.mie.eq(0)
        self.sync += self.mpie.eq(0)
        self.sync += self.mstatus.eq(0)


class MIE:
    def __init__(self, comb, sync):
        self.comb = comb
        self.sync = sync
        self.meie = Signal(name="mie_meie")
        self.mtie = Signal(name="mie_mtie")
        self.msie = Signal(name="mie_msie")
        self.mie = Signal(32)


class MIP:
    def __init__(self):
        self.mip = Signal(32)


class M:
    def __init__(self, comb, sync):
        self.comb = comb
        self.sync = sync
        self.mcause = Signal(32)
        self.mepc = Signal(32)
        self.mscratch = Signal(32)
        self.sync += self.mcause.eq(0)
        self.sync += self.mepc.eq(0) # 32'hXXXXXXXX;
        self.sync += self.mscratch.eq(0) # 32'hXXXXXXXX;


class Misa:
    def __init__(self, comb, sync):
        self.comb = comb
        self.sync = sync
        self.misa = Signal(32)
        cl = []
        for l in list(string.ascii_lowercase):
            value = 1 if l == 'i' else 0
            cl.append(Constant(value))
        cl.append(Constant(0, 4))
        cl.append(Constant(0b01, 2))
        self.comb += self.misa.eq(Cat(cl))


class Fetch:
    def __init__(self, comb, sync):
        self.comb = comb
        self.sync = sync
        self.action = Signal(fetch_action, name="fetch_action")
        self.target_pc = Signal(32, name="fetch_target_pc")
        self.output_pc = Signal(32, name="fetch_output_pc")
        self.output_instruction = Signal(32, name="fetch_ouutput_instruction")
        self.output_state = Signal(fetch_output_state,name="fetch_output_state")

class CSR:
    def __init__(self, comb, sync, dc, register_rs1):
        self.comb = comb
        self.sync = sync
        self.number = Signal(12, name="csr_number")
        self.input_value = Signal(32, name="csr_input_value")
        self.reads = Signal(name="csr_reads")
        self.writes = Signal(name="csr_writes")
        self.op_is_valid = Signal(name="csr_op_is_valid")

        self.comb += self.number.eq(dc.immediate)
        self.comb += self.input_value.eq(Mux(dc.funct3[2],
                                            dc.rs1,
                                            register_rs1))
        self.comb += self.reads.eq(dc.funct3[1] | (dc.rd != 0))
        self.comb += self.writes.eq(~dc.funct3[1] | (dc.rs1 != 0))

        self.comb += self.get_csr_op_is_valid()

    def get_csr_op_is_valid(self):
        """ determines if a CSR is valid
        """
        c = {}
        # invalid csrs
        for f in [csr_ustatus, csr_fflags, csr_frm, csr_fcsr,
                  csr_uie, csr_utvec, csr_uscratch, csr_uepc,
                  csr_ucause, csr_utval, csr_uip, csr_sstatus,
                  csr_sedeleg, csr_sideleg, csr_sie, csr_stvec,
                  csr_scounteren, csr_sscratch, csr_sepc, csr_scause,
                  csr_stval, csr_sip, csr_satp, csr_medeleg,
                  csr_mideleg, csr_dcsr, csr_dpc, csr_dscratch]:
            c[f] = self.op_is_valid.eq(0)

        # not-writeable -> ok
        for f in [csr_cycle, csr_time, csr_instret, csr_cycleh,
                  csr_timeh, csr_instreth, csr_mvendorid, csr_marchid,
                  csr_mimpid, csr_mhartid]:
            c[f] = self.op_is_valid.eq(~self.writes)

        # valid csrs
        for f in [csr_misa, csr_mstatus, csr_mie, csr_mtvec,
                  csr_mscratch, csr_mepc, csr_mcause, csr_mip]:
            c[f] = self.op_is_valid.eq(1)

        # not implemented / default
        for f in [csr_mcounteren, csr_mtval, csr_mcycle, csr_minstret,
                  csr_mcycleh, csr_minstreth, "default"]:
            c[f] = self.op_is_valid.eq(0)

        return Case(self.number, c)

    def evaluate_csr_funct3_op(self, funct3, previous, written):
        c = { "default": written.eq(Constant(0, 32))}
        for f in [F3.csrrw, F3.csrrwi]:
            c[f] = written.eq(self.input_value)
        for f in [F3.csrrs, F3.csrrsi]:
            c[f] = written.eq(self.input_value | previous)
        for f in [F3.csrrc, F3.csrrci]:
            c[f] = written.eq(~self.input_value & previous)
        return Case(funct3, c)


class MInfo:
    def __init__(self, comb):
        self.comb = comb
        # TODO
        self.cycle_counter   = Signal(64); # TODO: implement cycle_counter
        self.time_counter    = Signal(64); # TODO: implement time_counter
        self.instret_counter = Signal(64); # TODO: implement instret_counter

        self.mvendorid = Signal(32)
        self.marchid = Signal(32)
        self.mimpid = Signal(32)
        self.mhartid = Signal(32)
        self.comb += self.mvendorid.eq(Constant(0, 32))
        self.comb += self.marchid.eq(Constant(0, 32))
        self.comb += self.mimpid.eq(Constant(0, 32))
        self.comb += self.mhartid.eq(Constant(0, 32))

class Regs:
    def __init__(self, comb, sync):
        self.comb = comb
        self.sync = sync

        self.ra_en = Signal(reset=1, name="regfile_ra_en") # TODO: ondemand en
        self.rs1 = Signal(32, name="regfile_rs1")
        self.rs_a = Signal(5, name="regfile_rs_a")

        self.rb_en = Signal(reset=1, name="regfile_rb_en") # TODO: ondemand en
        self.rs2 = Signal(32, name="regfile_rs2")
        self.rs_b = Signal(5, name="regfile_rs_b")

        self.w_en = Signal(name="regfile_w_en")
        self.wval = Signal(32, name="regfile_wval")
        self.rd = Signal(32, name="regfile_rd")

class CPU(Module):
    """
    """

    def get_lsbm(self, dc):
        return Cat(Constant(1),
                   Mux((dc.funct3[1] | dc.funct3[0]),
                       Constant(1), Constant(0)),
                   Mux((dc.funct3[1]),
                       Constant(0b11, 2), Constant(0, 2)))

    # XXX this happens to get done by various self.sync actions
    #def reset_to_initial(self, m, mstatus, mie, registers):
    #    return [m.mcause.eq(0),
    #            ]

    def handle_trap(self, mcause, mepc, mie, mpie):
        s = [mcause.eq(self.new_mcause),
             mepc.eq(self.new_mepc),
             mpie.eq(self.new_mpie),
             mie.eq(self.new_mie)]
        return s

    def main_block(self, mtvec, mip, minfo, misa, csr, mi, m, mstatus, mie,
                         ft, dc,
                         load_store_misaligned,
                         loaded_value, alu_result,
                         lui_auipc_result):
        c = {}
        c[FOS.empty] = []
        c[FOS.trap] = self.handle_trap(m.mcause, m.mepc,
                                       mstatus.mie, mstatus.mpie)
        c[FOS.valid] = self.handle_valid(mtvec, mip, minfo, misa, csr, mi, m,
                                       mstatus, mie, ft, dc,
                                       load_store_misaligned,
                                       loaded_value,
                                       alu_result,
                                       lui_auipc_result)
        return [self.regs.w_en.eq(0),
                Case(ft.output_state, c),
                self.regs.w_en.eq(0)]

    def write_register(self, rd, val):
        return [self.regs.rd.eq(rd),
                self.regs.wval.eq(val),
                self.regs.w_en.eq(1)
               ]

    def handle_valid(self, mtvec, mip, minfo, misa, csr, mi, m, mstatus, mie,
                           ft, dc,
                           load_store_misaligned,
                           loaded_value, alu_result,
                           lui_auipc_result):
        # fetch action ack trap
        i = If((ft.action == FA.ack_trap) | (ft.action == FA.noerror_trap),
                self.handle_trap(m.mcause, m.mepc, mstatus.mie, mstatus.mpie)
              )

        # load
        i = i.Elif((dc.act & DA.load) != 0,
                If(~mi.rw_wait,
                    self.write_register(dc.rd, loaded_value)
                )
              )

        # op or op_immediate
        i = i.Elif((dc.act & DA.op_op_imm) != 0,
                self.write_register(dc.rd, alu_result)
              )

        # lui or auipc
        i = i.Elif((dc.act & DA.lui_auipc) != 0,
                self.write_register(dc.rd, lui_auipc_result)
              )

        # jal/jalr
        i = i.Elif((dc.act & (DA.jal | DA.jalr)) != 0,
                self.write_register(dc.rd, ft.output_pc + 4)
              )

        i = i.Elif((dc.act & DA.csr) != 0,
                self.handle_csr(mtvec, mip, minfo, misa, mstatus, mie, m,
                                dc, csr)
              )

        # fence, store, branch
        i = i.Elif((dc.act & (DA.fence | DA.fence_i |
                              DA.store | DA.branch)) != 0,
                # do nothing
              )

        return i

    def handle_csr(self, mtvec, mip, minfo, misa, mstatus, mie, m, dc, csr):
        csr_output_value = Signal(32)
        csr_written_value = Signal(32)
        c = {}

        # cycle
        c[csr_cycle]  = csr_output_value.eq(minfo.cycle_counter[0:32])
        c[csr_cycleh] = csr_output_value.eq(minfo.cycle_counter[32:64])
        # time
        c[csr_time]  = csr_output_value.eq(minfo.time_counter[0:32])
        c[csr_timeh] = csr_output_value.eq(minfo.time_counter[32:64])
        # instret
        c[csr_instret]  = csr_output_value.eq(minfo.instret_counter[0:32])
        c[csr_instreth] = csr_output_value.eq(minfo.instret_counter[32:64])
        # mvendorid/march/mimpl/mhart
        c[csr_mvendorid] = csr_output_value.eq(minfo.mvendorid)
        c[csr_marchid  ] = csr_output_value.eq(minfo.marchid  )
        c[csr_mimpid   ] = csr_output_value.eq(minfo.mimpid   )
        c[csr_mhartid  ] = csr_output_value.eq(minfo.mhartid  )
        # misa
        c[csr_misa     ] = csr_output_value.eq(misa.misa)
        # mstatus
        c[csr_mstatus  ] = [
            csr_output_value.eq(mstatus.mstatus),
            csr.evaluate_csr_funct3_op(dc.funct3, csr_output_value,
                                                  csr_written_value),
            mstatus.mpie.eq(csr_written_value[7]),
            mstatus.mie.eq(csr_written_value[3])
        ]
        # mie
        c[csr_mie      ] = [
            csr_output_value.eq(mie.mie),
            csr.evaluate_csr_funct3_op(dc.funct3, csr_output_value,
                                                  csr_written_value),
            mie.meie.eq(csr_written_value[11]),
            mie.mtie.eq(csr_written_value[7]),
            mie.msie.eq(csr_written_value[3]),
        ]
        # mtvec
        c[csr_mtvec    ] = csr_output_value.eq(mtvec)
        # mscratch
        c[csr_mscratch ] = [
            csr_output_value.eq(m.mscratch),
            csr.evaluate_csr_funct3_op(dc.funct3, csr_output_value,
                                                  csr_written_value),
            If(csr.writes,
                m.mscratch.eq(csr_written_value),
            )
        ]
        # mepc
        c[csr_mepc ] = [
            csr_output_value.eq(m.mepc),
            csr.evaluate_csr_funct3_op(dc.funct3, csr_output_value,
                                                  csr_written_value),
            If(csr.writes,
                m.mepc.eq(csr_written_value),
            )
        ]

        # mcause
        c[csr_mcause ] = [
            csr_output_value.eq(m.mcause),
            csr.evaluate_csr_funct3_op(dc.funct3, csr_output_value,
                                                  csr_written_value),
            If(csr.writes,
                m.mcause.eq(csr_written_value),
            )
        ]

        # mip
        c[csr_mip  ] = [
            csr_output_value.eq(mip.mip),
            csr.evaluate_csr_funct3_op(dc.funct3, csr_output_value,
                                                  csr_written_value),
        ]

        return [Case(csr.number, c),
                If(csr.reads,
                    self.write_register(dc.rd, csr_output_value)
                )]

    def __init__(self):
        Module.__init__(self)
        self.clk = ClockSignal()
        self.reset = ResetSignal()
        self.tty_write = Signal()
        self.tty_write_data = Signal(8)
        self.tty_write_busy = Signal()
        self.switch_2 = Signal()
        self.switch_3 = Signal()
        self.led_1 = Signal()
        self.led_3 = Signal()

        ram_size = Constant(0x8000)
        ram_start = Constant(0x10000, 32)
        reset_vector = Signal(32)
        mtvec = Signal(32)

        reset_vector.eq(ram_start)
        mtvec.eq(ram_start + 0x40)

        self.regs = Regs(self.comb, self.sync)

        rf = Instance("RegFile", name="regfile",
           i_ra_en = self.regs.ra_en,
           i_rb_en = self.regs.rb_en,
           i_w_en = self.regs.w_en,
           o_read_a = self.regs.rs1,
           o_read_b = self.regs.rs2,
           i_writeval = self.regs.wval,
           i_rs_a = self.regs.rs_a,
           i_rs_b = self.regs.rs_b,
           i_rd = self.regs.rd)

        self.specials += rf

        mi = MemoryInterface()

        mii = Instance("cpu_memory_interface", name="memory_instance",
                    p_ram_size = ram_size,
                    p_ram_start = ram_start,
                    i_clk=ClockSignal(),
                    i_rst=ResetSignal(),
                    i_fetch_address = mi.fetch_address,
                    o_fetch_data = mi.fetch_data,
                    o_fetch_valid = mi.fetch_valid,
                    i_rw_address = mi.rw_address,
                    i_rw_byte_mask = mi.rw_byte_mask,
                    i_rw_read_not_write = mi.rw_read_not_write,
                    i_rw_active = mi.rw_active,
                    i_rw_data_in = mi.rw_data_in,
                    o_rw_data_out = mi.rw_data_out,
                    o_rw_address_valid = mi.rw_address_valid,
                    o_rw_wait = mi.rw_wait,
                    o_tty_write = self.tty_write,
                    o_tty_write_data = self.tty_write_data,
                    i_tty_write_busy = self.tty_write_busy,
                    i_switch_2 = self.switch_2,
                    i_switch_3 = self.switch_3,
                    o_led_1 = self.led_1,
                    o_led_3 = self.led_3
                  )
        self.specials += mii

        ft = Fetch(self.comb, self.sync)

        fs = Instance("CPUFetchStage", name="fetch_stage",
            i_clk=ClockSignal(),
            i_rst=ResetSignal(),
            o_memory_interface_fetch_address = mi.fetch_address,
            i_memory_interface_fetch_data = mi.fetch_data,
            i_memory_interface_fetch_valid = mi.fetch_valid,
            i_fetch_action = ft.action,
            i_target_pc = ft.target_pc,
            o_output_pc = ft.output_pc,
            o_output_instruction = ft.output_instruction,
            o_output_state = ft.output_state,
            i_reset_vector = reset_vector,
            i_mtvec = mtvec,
        )
        self.specials += fs

        dc = Decoder()

        cd = Instance("CPUDecoder", name="decoder",
            i_instruction = ft.output_instruction,
            o_funct7 = dc.funct7,
            o_funct3 = dc.funct3,
            o_rd = dc.rd,
            o_rs1 = dc.rs1,
            o_rs2 = dc.rs2,
            o_immediate = dc.immediate,
            o_opcode = dc.opcode,
            o_decode_action = dc.act
        )
        self.specials += cd

        self.comb += self.regs.rs_a.eq(dc.rs1)
        self.comb += self.regs.rs_b.eq(dc.rs2)

        load_store_address = Signal(32)
        load_store_address_low_2 = Signal(2)
        load_store_misaligned = Signal()
        unmasked_loaded_value = Signal(32)
        loaded_value = Signal(32)

        lsc = Instance("CPULoadStoreCalc", name="cpu_loadstore_calc",
            i_dc_immediate = dc.immediate,
            i_dc_funct3 = dc.funct3,
            i_rs1 = self.regs.rs1,
            i_rs2 = self.regs.rs2,
            i_rw_data_in = mi.rw_data_in,
            i_rw_data_out = mi.rw_data_out,
            o_load_store_address = load_store_address,
            o_load_store_address_low_2 = load_store_address_low_2,
            o_load_store_misaligned = load_store_misaligned,
            o_loaded_value = loaded_value)

        self.specials += lsc

        # XXX rwaddr not 31:2 any more
        self.comb += mi.rw_address.eq(load_store_address[2:])

        unshifted_load_store_byte_mask = Signal(4)

        self.comb += unshifted_load_store_byte_mask.eq(self.get_lsbm(dc))

        # XXX yuck.  this will cause migen simulation to fail
        # (however conversion to verilog works)
        self.comb += mi.rw_byte_mask.eq(
                _Operator("<<", [unshifted_load_store_byte_mask,
                                        load_store_address_low_2]))

        self.comb += mi.rw_active.eq(~self.reset
                        & (ft.output_state == FOS.valid)
                        & ~load_store_misaligned
                        & ((dc.act & (DA.load | DA.store)) != 0))

        self.comb += mi.rw_read_not_write.eq(~dc.opcode[5])

        # alu
        alu_a = Signal(32)
        alu_b = Signal(32)
        alu_result = Signal(32)

        self.comb += alu_a.eq(self.regs.rs1)
        self.comb += alu_b.eq(Mux(dc.opcode[5],
                                  self.regs.rs2,
                                  dc.immediate))

        ali = Instance("cpu_alu", name="alu",
            i_funct7 = dc.funct7,
            i_funct3 = dc.funct3,
            i_opcode = dc.opcode,
            i_a = alu_a,
            i_b = alu_b,
            o_result = alu_result
        )
        self.specials += ali

        lui_auipc_result = Signal(32)
        self.comb += lui_auipc_result.eq(Mux(dc.opcode[5],
                                             dc.immediate,
                                             dc.immediate + ft.output_pc))

        self.comb += ft.target_pc.eq(Cat(0,
                    Mux(dc.opcode != OP.jalr,
                                ft.output_pc[1:32],
                                self.regs.rs1[1:32] + dc.immediate[1:32])))

        misaligned_jump_target = Signal()
        self.comb += misaligned_jump_target.eq(ft.target_pc[1])
 
        branch_arg_a = Signal(32)
        branch_arg_b = Signal(32)
        self.comb += branch_arg_a.eq(Cat( self.regs.rs1[0:31],
                                          self.regs.rs1[31] ^ ~dc.funct3[1]))
        self.comb += branch_arg_b.eq(Cat( self.regs.rs2[0:31],
                                          self.regs.rs2[31] ^ ~dc.funct3[1]))

        branch_taken = Signal()
        self.comb += branch_taken.eq(dc.funct3[0] ^
                                     Mux(dc.funct3[2],
                                         branch_arg_a < branch_arg_b,
                                         branch_arg_a == branch_arg_b))

        m = M(self.comb, self.sync)
        mstatus = MStatus(self.comb, self.sync)
        mie = MIE(self.comb, self.sync)
        misa = Misa(self.comb, self.sync)
        mip = MIP()

        mp = Instance("CPUMIP", name="cpu_mip",
            o_mip = mip.mip)

        self.specials += mp

        mii = Instance("CPUMIE", name="cpu_mie",
            o_mie = mie.mie,
            i_meie = mie.meie,
            i_mtie = mie.mtie,
            i_msie = mie.msie)

        self.specials += mii

        ms = Instance("CPUMStatus", name="cpu_mstatus",
            o_mstatus = mstatus.mstatus,
            i_mpie = mstatus.mpie,
            i_mie = mstatus.mie)

        self.specials += ms

        # CSR decoding
        csr = CSR(self.comb, self.sync, dc, self.regs.rs1)

        fi = Instance("CPUFetchAction", name="cpu_fetch_action",
            o_fetch_action = ft.action,
            i_output_state = ft.output_state,
            i_dc_act = dc.act,
            i_load_store_misaligned = load_store_misaligned,
            i_mi_rw_wait = mi.rw_wait,
            i_mi_rw_address_valid = mi.rw_address_valid,
            i_branch_taken = branch_taken,
            i_misaligned_jump_target = misaligned_jump_target,
            i_csr_op_is_valid = csr.op_is_valid)

        self.specials += fi

        minfo = MInfo(self.comb)

        self.new_mcause = Signal(32)
        self.new_mepc = Signal(32)
        self.new_mpie = Signal()
        self.new_mie = Signal()

        ht = Instance("CPUHandleTrap", "cpu_handle_trap",
                      i_ft_action = ft.action,
                      i_ft_output_pc = ft.output_pc,
                      i_dc_action = dc.act,
                      i_dc_immediate = dc.immediate,
                      i_load_store_misaligned = load_store_misaligned,
                      i_mie = mstatus.mie,
                      o_mcause = self.new_mcause,
                      o_mepc = self.new_mepc,
                      o_mpie = self.new_mpie,
                      o_mie = self.new_mie)

        self.specials += ht

        self.sync += If(~self.reset,
                        self.main_block(mtvec, mip, minfo, misa, csr, mi, m,
                                        mstatus, mie, ft, dc,
                                        load_store_misaligned,
                                        loaded_value,
                                       alu_result,
                                       lui_auipc_result)
                     )

if __name__ == "__main__":
    example = CPU()
    print(verilog.convert(example,
         {
           example.tty_write,
           example.tty_write_data,
           example.tty_write_busy,
           example.switch_2,
           example.switch_3,
           example.led_1,
           example.led_3,
           }))
