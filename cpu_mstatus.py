"""
/*
 * Copyright 2018 Jacob Lifshay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
`timescale 1ns / 1ps
`include "riscv.vh"
`include "cpu.vh"
"""

import string
from migen import *
from migen.fhdl import verilog
from migen.fhdl.structure import _Operator

from riscvdefs import *
from cpudefs import *

class MStatus:
    def __init__(self):
        self.mpie = Signal(name="mstatus_mpie", reset=0)
        self.mie = Signal(name="mstatus_mie", reset=0)
        self.mprv = Signal(name="mstatus_mprv", reset=0)
        self.tsr = Signal(name="mstatus_tsr", reset=0)
        self.tw = Signal(name="mstatus_tw", reset=0)
        self.tvm = Signal(name="mstatus_tvm", reset=0)
        self.mxr = Signal(name="mstatus_mxr", reset=0)
        self._sum = Signal(name="mstatus_sum", reset=0)
        self.xs = Signal(name="mstatus_xs", reset=0)
        self.fs = Signal(name="mstatus_fs", reset=0)
        self.mpp = Signal(2, name="mstatus_mpp", reset=0b11)
        self.spp = Signal(name="mstatus_spp", reset=0)
        self.spie = Signal(name="mstatus_spie", reset=0)
        self.upie = Signal(name="mstatus_upie", reset=0)
        self.sie = Signal(name="mstatus_sie", reset=0)
        self.uie = Signal(name="mstatus_uie", reset=0)

        io = set()
        for n in dir(self):
            if n.startswith("_"):
                continue
            n = getattr(self, n)
            if not isinstance(n, Signal):
                continue
            io.add(n)
        self.io = io


class CPUMStatus(Module, MStatus):

    def __init__(self):
        MStatus.__init__(self)
        Module.__init__(self)

        self.mstatus = Signal(32)

        for io in self.io:
            if io.name_override != self.mpp.name_override:
                self.comb += io.eq(0x0)
        self.comb += self.mpp.eq(0b11)
        self.comb += self.mstatus.eq(self.make())

        self.io = set({self.mstatus, self.mpie, self.mie})

    def make(self):
        return Cat(
                self.uie, self.sie, Constant(0), self.mie,
                self.upie, self.spie, Constant(0), self.mpie,
                self.spp, Constant(0, 2), self.mpp,
                self.fs, self.xs, self.mprv, self._sum,
                self.mxr, self.tvm, self.tw, self.tsr,
                Constant(0, 8),
                (self.xs == Constant(0b11, 2)) | (self.fs == Constant(0b11, 2))
                )

if __name__ == "__main__":
    example = CPUMStatus()
    print(verilog.convert(example, example.io))
