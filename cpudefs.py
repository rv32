"""
/*
 * Copyright 2018 Jacob Lifshay
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 */
"""

from migen import Constant
fetch_action = 3

class FA:
    """ Fetch action constants
    """
    default = Constant(0x0, fetch_action)
    fence = Constant(0x1, fetch_action)
    jump = Constant(0x2, fetch_action)
    wait = Constant(0x3, fetch_action)
    error_trap = Constant(0x4, fetch_action)
    noerror_trap = Constant(0x5, fetch_action)
    ack_trap = Constant(0x6, fetch_action)

fetch_output_state = 2

class FOS:
    """ Fetch output state constants
    """
    empty = Constant(0x0, fetch_output_state)
    valid = Constant(0x1, fetch_output_state)
    trap = Constant(0x2, fetch_output_state)

decode_action = 12

class DA:
    """ Decode action constants
    """
    trap_illegal_instruction = Constant(0x1, decode_action)
    load = Constant(0x2, decode_action)
    fence = Constant(0x4, decode_action)
    fence_i = Constant(0x8, decode_action)
    op_op_imm = Constant(0x10, decode_action)
    lui_auipc = Constant(0x20, decode_action)
    store = Constant(0x40, decode_action)
    branch = Constant(0x80, decode_action)
    jalr = Constant(0x100, decode_action)
    jal = Constant(0x200, decode_action)
    trap_ecall_ebreak = Constant(0x400, decode_action)
    csr = Constant(0x800, decode_action)
